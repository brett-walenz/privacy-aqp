/* -------------------------------------------------------------------------
 *
 * iceberg.c
 *
 * This is the code from the "Optimizing Iceberg Queries with Complex Joins" paper. I'm leaving it here as there may be tricks/techniques we want to refer back to. - brett 
 *
 * -------------------------------------------------------------------------
 */
#include "postgres.h"

#include "libpq/auth.h"
#include "parser/analyze.h"
#include "utils/ruleutils.h"
#include "utils/guc.h"
#include "utils/timestamp.h"
#include "lib/stringinfo.h"
#include "catalog/pg_operator.h"
#include "access/htup.h"
#include "access/htup_details.h"
#include "utils/syscache.h"
#include "utils/lsyscache.h"
#include "storage/fd.h"
#include "optimizer/tlist.h"

PG_MODULE_MAGIC;

void		_PG_init(void);

typedef struct IcebergRewrittenQuery {
   List *ungrounded;
   List *cache_vars;
   List *variables;
   List *cache_types;
   List *prime_variables;
   List *group_tles;
   StringInfo aliasBuf;
   char *min_bool;

   char *workload_query;
   char *bindings_query;
   char *cache_name;
   char *cache_create;
   char *cache_select;
   char *cache_insert;
   char *rewritten_query;
   char *custom_function;

} IcebergRewrittenQuery;

static IcebergRewrittenQuery * new_IRQ() {
    IcebergRewrittenQuery *irq;
	irq = (IcebergRewrittenQuery *) palloc(sizeof(IcebergRewrittenQuery));
    irq->ungrounded = NIL;
    irq->cache_vars = NIL;
    irq->cache_types = NIL;
    irq->prime_variables = NIL;
    irq->group_tles = NIL;
    irq->aliasBuf = makeStringInfo();
    return irq;
}

/* Original Hook */

static char * get_expression_symbol(Oid opno) {
	HeapTuple	opertup;
	Form_pg_operator operform;
    char *tmp;
    opertup = SearchSysCache1(OPEROID, ObjectIdGetDatum(opno));
	if (!HeapTupleIsValid(opertup))
		        elog(ERROR, "cache lookup failed for operator %u", opno);
	operform = (Form_pg_operator) GETSTRUCT(opertup);
	tmp = NameStr(operform->oprname);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: tmp is %s", tmp)));
    return tmp;
}

static char *get_DataType(Query *query, Var *var) {
    /* TODO: make this work on the actual data types. */ 
    return "INTEGER";
}

static char *get_colname(Query *query, int relation_num, int var_index) {
     ListCell *curr;
     RangeTblEntry *rte;
     List *col_names;

     rte = (RangeTblEntry *) list_nth(query->rtable, relation_num-1);
     col_names = ((Alias *) rte->eref)->colnames;
     curr = list_nth(col_names, var_index-1);
     return strVal(curr);
}

static char *get_colname_var(Query *query, Var *var) {
    return get_colname(query, var->varno, var->varattno);
}

static char *get_relname(Query *query, int driver_relation) {
    RangeTblEntry *rte;
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("get_relname, rtable size %d", list_length(query->rtable))));

    rte = (RangeTblEntry *) list_nth(query->rtable, driver_relation-1);
    return get_rel_name(rte->relid);
}

static char *get_alias_rte(RangeTblEntry *rte) {
    return ((Alias *) rte->eref)->aliasname;
}

static char *get_alias(Query *query, int relation_num) {
    RangeTblEntry *rte;
    rte = (RangeTblEntry *) list_nth(query->rtable, relation_num-1);
    return get_alias_rte(rte);
}

static char *get_Var_Str(ParseState *pstate, Query *query, Var *var, IcebergRewrittenQuery *irq, int driver_relation, bool imply_statement) {
    Oid relation_num;
    Oid var_index;
    char *column;
    bool grounded;
    Value *value;

    StringInfoData buf;
	initStringInfo(&buf);

    grounded = false;
    relation_num = var->varno;
    if (relation_num == driver_relation) {
        grounded = true;
    }
    var_index = var->varattno;
    /* Look up the column name by using the relation_num and var_index in the Query object. */ 
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("column info relation %d, var_index %d", relation_num, var_index)));
    column = get_colname(query, relation_num, var_index);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("column info2 %s", column)));
    if (!grounded) {
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("after bool check")));
        appendStringInfo(&buf, "G%s", column);
        value = makeString(buf.data);
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("value %s", strVal(value))));
        if (irq->ungrounded == NIL) {
            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("is ungrounded null")));
            irq->ungrounded = list_make1(value);
        }
        else if (!list_member(irq->ungrounded, value)) {
            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ungrounded is not null")));
            irq->ungrounded = lappend(irq->ungrounded, value);
        }

    } else if (imply_statement) {
        appendStringInfo(&buf, "%s_prime", column);
        irq->prime_variables = lappend(irq->prime_variables, makeString(buf.data));
    } else {
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("in else statement")));
        appendStringInfo(&buf, "%s", column);
        if (irq->cache_vars == NIL) {
            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("irq cache vars is null")));
            irq->cache_vars = list_make1(var);
        } else {
            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("irq cache vars is not null")));
            irq->cache_vars = lappend(irq->cache_vars, var);
        }
    }

    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("buf.data %s", buf.data)));
    return buf.data;
}

static char* get_HavingExpr_Str(Query *query, IcebergRewrittenQuery *irq, bool negated) {
	//TODO: make this use the same code as get_OpExpr_Str
	//for now, we just assume COUNT(*) OP CONST
	Node *node;
	OpExpr *op;
	char *symbol;
    StringInfoData buf;
	char *const_value;
    ListCell *arg;
    Node *cValue;
    Datum datum;
    char *s;

    initStringInfo(&buf);
	node = query->havingQual;
	if (nodeTag(node) == T_OpExpr) {
            op = (OpExpr *)node;
            symbol = get_expression_symbol(op->opno);
            if (negated) {
                appendStringInfo(&buf, "NOT ");
            }
            appendStringInfo(&buf, "value %s ", symbol);
            foreach(arg, op->args) {
                cValue = lfirst(arg);
                if (nodeTag(cValue) == T_Agg) {

                } else if (nodeTag(cValue) == T_Const) {
                    Const *con = (Const *) cValue;
                    datum = con->constvalue;
                    s = (char *) (&datum);
                    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: HAVING clause const value %d", (int) s[0])));
                    appendStringInfo(&buf, "%d", (int) s[0]);
                }
            }


	}
    return buf.data;
}

static char *get_OpExpr_Str(ParseState *pstate, Query *query, Node *node, IcebergRewrittenQuery *irq, int driver_relation, bool imply_statement) {
    OpExpr *op;
    ListCell *arg;
    StringInfoData buf;
    Var *v;
    Node * cValue;
    RelabelType *t;
    char * symbol;
    char *column;
    int argNum = 0;
    initStringInfo(&buf);

   
    if (irq->aliasBuf->len > 0 && !imply_statement) {
        appendStringInfo(irq->aliasBuf, " AND ");
    }
    if (nodeTag(node) == T_OpExpr) {
        //T_OpExpr has two args, and one expression
        /* This is the base expression argument. */ 
        op = (OpExpr *)node;
        symbol = get_expression_symbol(op->opno); 
        /* For every arg, get the column name and create a grounded/ungrounded name for the bool expr. */ 
        foreach(arg, op->args) {
            
            cValue = lfirst(arg);

            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: cValue type is %d", nodeTag(cValue))));
            /* If it's a T_Relabel_Type, then the actual Var is hidden in cValue->arg, and might be of type Var or Const. */
            if (nodeTag(cValue) == T_RelabelType) {
                t = (RelabelType *) cValue;
                
                ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: Relabel type arg is %d", nodeTag(t->arg))));
                if (nodeTag(t->arg) == T_Var) {
                    v = (Var *)t->arg;
                    column = get_Var_Str(pstate, query, v, irq, driver_relation, imply_statement);
                }
            } else if (nodeTag(cValue) == T_Var) {
                column = get_Var_Str(pstate, query, (Var *) cValue, irq, driver_relation, imply_statement);
                ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("Getting alias for varno %d", ((Var *) cValue)->varno)));
                if (!imply_statement) {
                    appendStringInfo(irq->aliasBuf, "%s.%s", get_alias(query, ((Var *) cValue)->varno), get_colname(query, ((Var *) cValue)->varno, ((Var *) cValue)->varattno));
                }
            }

            appendStringInfo(&buf, "%s", column);
            
            /* This is just to keep track of when to insert the comparator symbol. */ 
            if (argNum == 0) {
                appendStringInfo(&buf, " %s ", symbol);
                if (!imply_statement) {
                    appendStringInfo(irq->aliasBuf, " %s ", symbol);
                }
            }
            argNum++;
        }

    }
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: opExprStr %s", buf.data)));
    return buf.data;

}

static char * get_BoolExpr_Str(ParseState *pstate, Query *query, Node *node, IcebergRewrittenQuery *irq, int driver_relation, bool imply_statement) {
    StringInfoData buf;
    BoolExpr * boolop;
    Node *expr;
    ListCell *arg;
    int argNum = 0;
    char *exprStr;

    initStringInfo(&buf);
 
    boolop = (BoolExpr *)node;
    
    foreach(arg, boolop->args) {
       expr = lfirst(arg);
       exprStr = get_OpExpr_Str(pstate, query, expr, irq, driver_relation, imply_statement);
       if (argNum > 0) {
            /* For now we assume conjunctive queries, but nothing is stopping a mixture. */ 
            appendStringInfo(&buf, " AND ");
       }
       appendStringInfo(&buf, "(%s)", exprStr); 
       argNum++;
    }
    return buf.data;

}

static void get_Cache_Vars(ParseState *pstate, Query *query, Node *node, IcebergRewrittenQuery *irq, int driver_relation) {
    Var *curr;
    ListCell *cell;
    foreach(cell, irq->cache_vars) {
        curr = (Var *) lfirst(cell);
        //get the type of the variable as a ddl string (INTEGER, VARCHAR, etc).
        irq->cache_types = lappend(irq->cache_types, makeString(get_DataType(query, curr)));
    }
    return;
}

static void get_GroupBy_Vars(Query *query, IcebergRewrittenQuery *irq) {

    ListCell *cell;
    foreach(cell, query->groupClause) {
	    SortGroupClause *groupcl = (SortGroupClause *) lfirst(cell);
		TargetEntry *tle = get_sortgroupclause_tle(groupcl, query->targetList);
        irq->group_tles = lappend(irq->group_tles, tle);
    }

}
static char * pipe_to_mathematica(Query *query, char *boolExpr, List *ungrounded, List *variables, List *prime_variables) {
    FILE *pp;
    StringInfoData buf;
    StringInfoData outBuf;
    char input[512];
    ListCell *curr;
    Node *n;

    initStringInfo(&buf);
    initStringInfo(&outBuf);

    appendStringInfo(&buf, "/home/bwalenz/anaconda3/bin/python /home/bwalenz/phd/research/queryopt/minimize/logic.py '%s' '", boolExpr);

    foreach(curr, ungrounded) {
        n = (Node *)lfirst(curr);
        appendStringInfo(&buf, "%s ", strVal(n));   
    }

    appendStringInfo(&buf, "' ");
    foreach(curr, ungrounded) {
        n = (Node *)lfirst(curr);
        appendStringInfo(&buf, "%s ", strVal(n));   
    }

    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("creating the list")));
    foreach(curr, variables) {
        n = (Node *)lfirst(curr);
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("outputting a variable")));
        appendStringInfo(&buf, "%s ", get_colname_var(query, (Var *)n));
    }
    
    foreach(curr, prime_variables) {
        n = (Node *)lfirst(curr);
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("outputting a variable")));
        appendStringInfo(&buf, "%s ", strVal(n));
    }
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("string to shell: %s", buf.data)));
    pp = OpenPipeStream(buf.data, "r");
    if (pp != NULL) {
        while (fgets(input, sizeof(input), pp) != NULL) {
            appendStringInfo(&outBuf, "%s", input);
        }
    }
    ClosePipeStream(pp);

    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("read in: %s", outBuf.data)));
    return outBuf.data;
}

static void pipe_to_file(char *rewritten_query) {
    FILE *pp;
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("writing to file")));
    pp = OpenPipeStream("/home/bwalenz/postgres/src/query.sql", "w");
    if (pp != NULL) {

        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("pp is not null")));
        fputs(rewritten_query, pp);
        fflush(pp);
    }

    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("closing pipe")));
    ClosePipeStream(pp);
}


static char * make_insert_statement(Query *query, IcebergRewrittenQuery *irq) {
    StringInfoData buf;
    ListCell *cache_cell;
    Var *cache_var;
    int varNum = 0;

    initStringInfo(&buf);
    appendStringInfo(&buf, "INSERT INTO skip_cache(");

    /* the table(a, b, c) definition part */ 
    foreach(cache_cell, irq->cache_vars) {
        cache_var = lfirst(cache_cell); 
        appendStringInfo(&buf, "%s", get_colname_var(query, cache_var));
        varNum++;
        if (varNum < list_length(irq->cache_vars)) {
           appendStringInfo(&buf, ", "); 
        }
    }

    appendStringInfo(&buf, ", value) VALUES (");

    /* the values({a}, {b}, {c}) part */ 
    varNum = 0;
    foreach(cache_cell, irq->cache_vars) {
        cache_var = lfirst(cache_cell); 
        appendStringInfo(&buf, "bindings.%s", get_colname_var(query, cache_var));
        varNum++;
        if (varNum < list_length(irq->cache_vars)) {
           appendStringInfo(&buf, ", "); 
        }
    }

    appendStringInfo(&buf, ", current_output.value");
    appendStringInfo(&buf, ");");
    return buf.data;
    
}

static char * make_select_statement(Query *query, IcebergRewrittenQuery *irq) {
    
    /* Here the boolExpr needs to already have the result constraint added in (ex. value > 50).*/ 
    StringInfoData buf;
    initStringInfo(&buf);
    appendStringInfo(&buf, "SELECT 1 INTO cache_value FROM skip_cache WHERE (%s) AND NOT(%s) LIMIT 1;", irq->min_bool, get_HavingExpr_Str(query, irq, false));
    return buf.data;
}

static char * make_create_statement(Query *query, IcebergRewrittenQuery *irq) {
    StringInfoData buf;
    Var *cache_var;
    ListCell *cache_cell;
    int varNum = 0;

    initStringInfo(&buf);
    appendStringInfo(&buf, "CREATE TEMP TABLE skip_cache(");

    foreach(cache_cell, irq->cache_vars) {
        cache_var = (Var *) lfirst(cache_cell);
        appendStringInfo(&buf, "%s %s", get_colname_var(query, cache_var), get_DataType(query, cache_var));
        varNum++;
        if (varNum < list_length(irq->cache_vars)) {
            appendStringInfo(&buf, ", ");
        }
    }
    appendStringInfo(&buf, ", value INTEGER");
    appendStringInfo(&buf, ");");
    return buf.data;
}

static char *make_bindings_statement(Query *query, IcebergRewrittenQuery *irq, int driver_relation)
{
    /* Grab the set of ungrounded variables from irq, get the col_names, and write a select query from the driver relation. */
    StringInfoData buf;
    Var *cache_var;
    ListCell *curr;
    TargetEntry *entry;

    initStringInfo(&buf);
    appendStringInfo(&buf, "SELECT ");
    foreach(curr, irq->group_tles) {
        entry = (TargetEntry *)lfirst(curr);
        appendStringInfo(&buf, "%s,", entry->resname);
    }
    foreach(curr, irq->cache_vars) {
        cache_var = (Var *) lfirst(curr);
        appendStringInfo(&buf, "%s", get_colname_var(query, cache_var));
        if (curr->next) {
        appendStringInfo(&buf, ",");
        }
    }

    appendStringInfo(&buf, " FROM ");

    appendStringInfo(&buf, "%s", get_relname(query, driver_relation)); 
    appendStringInfo(&buf, " WHERE ");
    foreach(curr, irq->cache_vars) {
        cache_var = (Var *) lfirst(curr);
        appendStringInfo(&buf, "%s IS NOT NULL", get_colname_var(query, cache_var));
        if (curr->next) {
            appendStringInfo(&buf, " AND ");
        }
    }
    //appendStringInfo(&buf, ";");
    
    return buf.data;
}

static char *make_workload_statement(Query *query, IcebergRewrittenQuery *irq, int driver_relation) 
{
    /* Walk the tree, output original statement, but remove HAVING clause, WHERE clause, and driver relation in FROM clause.
    Then, add a where clause with cache_vars = ? (specific binding) */
    StringInfoData buf;
    RangeTblEntry *tbl;
    ListCell *curr;

    Var *cache_var;
    char *colname;
    TargetEntry *entry;

    int relation_num = 1;
    initStringInfo(&buf);

    appendStringInfo(&buf, "SELECT COUNT(*)::int as value INTO current_output FROM");

    /* Iterate through all the RTE's and find ones that DO NOT match the driver_relation. */ 
    foreach(curr, query->rtable) {
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: iterate rtable, num: %d", relation_num)));
        if (relation_num > 1) {
            appendStringInfo(&buf, ", ");
        }
        tbl = (RangeTblEntry *) lfirst(curr);
        appendStringInfo(&buf, " %s", get_relname(query, relation_num));
        appendStringInfo(&buf, " %s", get_alias(query, relation_num));
        relation_num++;

    }

    appendStringInfo(&buf, " WHERE ");

    /* now, we need to generate our where condition. Walk throught he list of cache_vars, output an equality clause with a 
    placeholder. Placeholder will be bound and rewritten in executor function. */

    foreach (curr, irq->group_tles) {
        entry = (TargetEntry *)lfirst(curr);
        appendStringInfo(&buf, "%s.%s = bindings.%s AND ", get_alias(query, driver_relation), entry->resname, entry->resname);
    }
    
    foreach (curr, irq->cache_vars) {
        cache_var = (Var *) lfirst(curr);
        colname = get_colname_var(query, cache_var);
        appendStringInfo(&buf, "%s.%s = bindings.%s ", get_alias(query, driver_relation), colname, colname);
        if (curr->next) {
            appendStringInfo(&buf, " AND ");
        }
    }

    appendStringInfo(&buf, " AND ");
    appendStringInfo(&buf, "%s", irq->aliasBuf->data);
    appendStringInfo(&buf, ";");

    return buf.data; 
    
}

static char *make_final_statement(IcebergRewrittenQuery *irq) 
{
    StringInfoData buf;

    initStringInfo(&buf);
    appendStringInfo(&buf, "SELECT * FROM perturb('%s', '%s',", irq->bindings_query, irq->workload_query);
    appendStringInfo(&buf, " '%s', '%s', '%s', 'skip_cache') AS (cnt INT);", irq->cache_create, irq->cache_insert, irq->cache_select);
    return buf.data;
}

static char *make_PG_Func(Query *query, IcebergRewrittenQuery *irq, int driver_relation) {
    StringInfoData buf;

    initStringInfo(&buf);
    appendStringInfo(&buf, "CREATE OR REPLACE FUNCTION perturb() RETURNS SETOF RECORD AS $$\n");
    appendStringInfo(&buf, "DECLARE\n");

    appendStringInfo(&buf, "bindings RECORD;\n");
    appendStringInfo(&buf, "current_output RECORD;\n");
    appendStringInfo(&buf, "cache_value RECORD;\n");
    appendStringInfo(&buf, "skip_current BOOLEAN;\n");
    appendStringInfo(&buf, "BEGIN\n");
    appendStringInfo(&buf, "%s\n", make_create_statement(query, irq));
    appendStringInfo(&buf, "FOR bindings IN %s\n", make_bindings_statement(query, irq, driver_relation));
    appendStringInfo(&buf, "LOOP\n");
    appendStringInfo(&buf, "skip_current := FALSE;\n");
    appendStringInfo(&buf, "%s\n", make_select_statement(query, irq));
    appendStringInfo(&buf, "IF cache_value IS NOT NULL THEN\n");
    appendStringInfo(&buf, "skip_current := TRUE;\n");
    appendStringInfo(&buf, "END IF;\n");
    appendStringInfo(&buf, "IF NOT skip_current THEN\n");
    appendStringInfo(&buf, "%s\n", make_workload_statement(query, irq, driver_relation));
    appendStringInfo(&buf, "%s\n", make_insert_statement(query, irq));
    appendStringInfo(&buf, "IF current_output IS NOT NULL AND current_output.%s THEN\n", get_HavingExpr_Str(query, irq, false));
    appendStringInfo(&buf, "RETURN NEXT current_output;\n");
    appendStringInfo(&buf, "END IF;\nEND IF;\nEND LOOP;\n");
    appendStringInfo(&buf, "DROP TABLE skip_cache;\nEND;\n");
    appendStringInfo(&buf, "$$ LANGUAGE plpgsql;\n");
    return buf.data;

}


static post_parse_analyze_hook_type original_post_parse_analyze_hook  = NULL;

/***
 Given a query tree, this function analyzes the tree and determines whether or not it is suitable
 for modifications as in our Optimizations for Complex Iceberg Queries paper.
 
 We first check this by a series of conditions (outlined in the paper). 
 Currently only HAVING clauses. TODO: add the more complex conditions.
 
 We currently operate on two join tables only, although it is possible to operate on
 more than two. Currently we only implement pruning I believe.
 
 1. Start from an analyzed tree
 2. Reconstruct the query join expression.    
 3. Form a logical implication NOT(expr) OR (impl_expr)
 4. Send logical implication to mathematica
 5. Search mathematica result for equation in terms of just variables and prime_variables
 6. Get the cache variables, and the group by variables
 7. make create, insert, and select statements
 8. make bindings, workload, and final statement
 9. make a clean postgres function from 7, 8
 10. TODO: execute the postgres function somehow

 ***/ 

static void iceberg_analyze_parse_tree(ParseState *pstate, Query *query)
{
    Node *node;
    char *boolExpr;
    char *nodeOut;
    char *boolImplExpr;
    int driver_relation = 2;
    StringInfoData buf;

    char *minBool;

    IcebergRewrittenQuery *irq;
    if (!query->havingQual) {
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: NO having qual, skipping query.")));
        return;
    }
    irq = new_IRQ();

    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: created irq")));
    initStringInfo(&buf);
    if (query->jointree) {
        /* Get the join tree qual node. */ 
        node = query->jointree->quals;
        nodeOut = nodeToString(node);
        if (nodeTag(node) == T_BoolExpr) {
            boolExpr = get_BoolExpr_Str(pstate, query, node, irq, driver_relation, false);
            boolImplExpr = get_BoolExpr_Str(pstate, query, node, irq, driver_relation, true);
        } else if (nodeTag(node) == T_OpExpr) {
            boolExpr = get_OpExpr_Str(pstate, query, node, irq, driver_relation, false);
            boolImplExpr = get_OpExpr_Str(pstate, query, node, irq, driver_relation, true);
        } 

        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: node string is %s", nodeOut)));
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: expression is %s", boolExpr)));
    }

    /* Form a logical implication. */ 
    appendStringInfo(&buf, "NOT(%s)", boolExpr);
    appendStringInfo(&buf, " OR (%s)", boolImplExpr);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: logical implication is %s", buf.data)));
    /* Now that we have a boolean expression, send it to mathematica and get back a new boolean expression. */
    minBool = pipe_to_mathematica(query, buf.data, irq->ungrounded, irq->cache_vars, irq->prime_variables);
    irq->min_bool = minBool;

    /* build our cache insert and create strings. */
    get_Cache_Vars(pstate, query, query->jointree->quals, irq, driver_relation);
    get_GroupBy_Vars(query, irq);
    irq->cache_create = make_create_statement(query, irq);
    irq->cache_insert = make_insert_statement(query, irq);

    irq->cache_select = make_select_statement(query, irq);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG statements: create %s", irq->cache_create)));
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG statements: select %s", irq->cache_select)));
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG statements: insert %s", irq->cache_insert)));
    /* now do the actual query rewriting and processing. */ 
    irq->bindings_query = make_bindings_statement(query, irq, driver_relation);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG statements: bindings %s", irq->bindings_query)));
    irq->workload_query = make_workload_statement(query, irq, driver_relation);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG statements: workload %s", irq->workload_query)));
    irq->rewritten_query = make_final_statement(irq);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG statements: rewritten %s", irq->rewritten_query)));
    //pipe_to_file(irq->rewritten_query);
    get_HavingExpr_Str(query, irq, true);
    irq->custom_function = make_PG_Func(query, irq, driver_relation);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG statements: custom_function %s", irq->custom_function)));


    /* Lastly, we need to dump the rewritten statement to a file, and exec it. */ 


}



/*
 * Module Load Callback
 */
void
_PG_init(void)
{
	/* Install Hooks */
	original_post_parse_analyze_hook = post_parse_analyze_hook;
	post_parse_analyze_hook = iceberg_analyze_parse_tree;
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("ICEBERG: Iceberg module initialized, hook set.")));
}
